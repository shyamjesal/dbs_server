from app import app,db
from app.models import Attendance
from flask import request,jsonify
@app.route('/')
@app.route('/index')
def index():
    return "Hello, World!"

@app.route('/attend',methods = ['POST'])
def attend():
   if request.method == 'POST':
      print(request)
      qr = request.form['qr']
      sid = request.form['id']
      course=request.form['course']
      semester=request.form['semester']
      evaltype=request.form['eval']
      room=request.form['room']
      invigilator = request.form['invigilator']

      evalRoom = EvalRoom.query.filter_by(code=course,semester=semester,type=evaltype,room=room,invigilator=invigilator).first()
      if evalRoom is None:
      	evalRoom = EvalRoom(code=course,semester=semester,type=evaltype,room=room,invigilator=invigilator)
      	db.session.add(evalRoom)
      	db.session.commit()

      a = Attendance(qr=qr, id=sid, code=course, semester=semester, type=evaltype)
      db.session.add(a)
      db.session.commit()
      response = dict()
      response["result"]=1
      return jsonify(response)
   else:
   	  response["result"]=0
      return jsonify(response)


@app.route('/marks',methods = ['POST'])
def marks():
   if request.method == 'POST':
      print(request)
      response = dict()
      qr = request.form['qr']
      marks = request.form['marks']
      course=request.form['course']
      semester=request.form['semester']
      evaltype=request.form['eval']

      attendance = Attendance.query.filter_by(qr=qr).first()
      if attendance is not None:
      	sid = attendance.id
      	marks_entry = StudentEval(id=sid,code=course,semester=semester,type=evaltype, marks=marks) 
      	db.session.add(marks_entry)
		db.session.commit()
      	response["result"]=1
      else:
		response["result"]=0

      return jsonify(response)
   else:
   	  response["result"]=0
      return jsonify(response)