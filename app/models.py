from datetime import datetime
from app import db

Base = db.Model
metadata = Base.metadata


class Course(Base):
    __tablename__ = 'course'

    code = db.Column(db.Text, primary_key=True)
    credits = db.Column(db.Integer, nullable=False)
    cname = db.Column(db.Text, nullable=False)


class CourseSem(Base):
    __tablename__ = 'course_sem'

    code = db.Column(db.Text, primary_key=True, nullable=False)
    semester = db.Column(db.Integer, primary_key=True, nullable=False)
    ic = db.Column(db.Text, nullable=False)


class Professor(Base):
    __tablename__ = 'professor'

    pid = db.Column(db.Text, primary_key=True)
    pname = db.Column(db.Text, nullable=False)


class Student(Base):
    __tablename__ = 'student'

    id = db.Column(db.Text, primary_key=True)
    name = db.Column(db.Text, nullable=False)


class Attendance(Base):
    __tablename__ = 'attendance'

    qr = db.Column(db.Integer, primary_key=True, nullable=False)
    id = db.Column(db.ForeignKey('student.id'), nullable=False)
    code = db.Column(db.ForeignKey('evaluation.code'))
    semester = db.Column(db.ForeignKey('evaluation.semester'))
    type = db.Column(db.Text)

    student = db.relationship('Student')
    evaluation = db.relationship('Evaluation', primaryjoin='Attendance.code == Evaluation.code')
    evaluation1 = db.relationship('Evaluation', primaryjoin='Attendance.semester == Evaluation.semester')


class Evaluation(Base):
    __tablename__ = 'evaluation'
    __table_args__ = (
        db.
       ForeignKeyConstraint(['code', 'semester'], ['course_sem.code', 'course_sem.semester']),
    )

    code = db.Column(db.Text, primary_key=True, nullable=False)
    semester = db.Column(db.Integer, primary_key=True, nullable=False)
    type = db.Column(db.Text, primary_key=True, nullable=False)
    max_marks = db.Column(db.Integer, server_default=db.text("Null"))
    date = db.Column(db.Integer, nullable=False)

    course_sem = db.relationship('CourseSem')


class StudentEval(Base):
    __tablename__ = 'student_eval'

    id = db.Column(db.ForeignKey('student.id'), primary_key=True, nullable=False)
    code = db.Column(db.Text, primary_key=True, nullable=False)
    semester = db.Column(db.Integer, primary_key=True, nullable=False)
    type = db.Column(db.Text, primary_key=True, nullable=False)
    marks = db.Column(db.Integer)

    student = db.relationship('Student')


class Study(Base):
    __tablename__ = 'studies'

    id = db.Column(db.
    ForeignKey('student.id'), primary_key=True, nullable=False)
    code = db.Column(db.
    ForeignKey('course_sem.code'), primary_key=True, nullable=False)
    semester = db.Column(db.Integer, primary_key=True, nullable=False)
    grade = db.Column(db.Integer, server_default=db.text("NULL"))

    course_sem = db.relationship('CourseSem')
    student = db.relationship('Student')


class Teach(Base):
    __tablename__ = 'teaches'

    pid = db.Column(db.
    ForeignKey('professor.pid'), primary_key=True, nullable=False)
    code = db.Column(db.
    ForeignKey('course_sem.code'), primary_key=True, nullable=False)
    sem = db.Column(db.Integer, primary_key=True, nullable=False)

    course_sem = db.relationship('CourseSem')
    professor = db.relationship('Professor')


class EvalRoom(Base):
    __tablename__ = 'eval_room'

    code = db.Column(db.ForeignKey('evaluation.code'), primary_key=True, nullable=False)
    semester = db.Column(db.ForeignKey('evaluation.semester'), primary_key=True, nullable=False)
    type = db.Column(db.ForeignKey('evaluation.type'), primary_key=True, nullable=False)
    room = db.Column(db.Text, primary_key=True, nullable=False)
    invigilator = db.Column(db.Text, nullable=False)

    evaluation = db.relationship('Evaluation', primaryjoin='EvalRoom.code == Evaluation.code')
    evaluation1 = db.relationship('Evaluation', primaryjoin='EvalRoom.semester == Evaluation.semester')
    evaluation2 = db.relationship('Evaluation', primaryjoin='EvalRoom.type == Evaluation.type')

    